const button = document.querySelector('button');
console.log("Hello once more")


for(let i = 0; i < 4; i++){
	button.addEventListener('click', function(){
	console.log(i)
})

}

button.innerHTML = "<a><i>This is a button</i></a>";

let hello;
console.log(hello); //result: undefined

//console.log(x); // result: err
//let x;

// Declaration Variables
let firstName = "Jin"

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const pi = 3.14;
console.log(pi);

a = 5;
console.log(a);
var a;

let b;
b = 6;
console.log(b);


let outerVar = "hello";

{
	let innerVar = "hello again";
	console.log(innerVar);
}

console.log(outerVar);




let myName = '"Christian"';
let greeting = "Hi I'm " + myName;
console.log(greeting)

console.log('console.log(greeting)')

// escape character (\)
// "\n"

let mailAddress = "Metro Manila\n\nPhilipines"
console.log(mailAddress)

console.log(69)

// Arrays



//similar data types

let anime = ['naruto', 'bleach', 'pokemon', 'attack on titan'];
console.log(anime)

//different data types;
let random = ['JK', 23, true];
console.log(random);

//object data type
/*
syntax: 
	let/const object name = {
	propertyA : valueA,
	propertyB : valueB
	}
*/

let food = {
	tasteGood: false,
	name: "Banana",
	quantity: 9,
	marketPlace: ["Divisoria", "Mall", "Friend's house"],
	otherType:{
		name: ["sab'a", "lakatan"],
		quantity: 194,
		marketPlace: ["Divisoria", "Mall", "Friend's house"],
		tasteGood: true,
	}
}
console.log(food.otherType.name[1]);

//typeof operator
console.log(typeof food); //result Object
console.log(typeof asd)
random[1] = 29;
console.log(random)